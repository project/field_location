const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const { configure } = require('./webpack.config.common');

module.exports = configure(
  'development',
  'cheap-module-eval-source-map',
  [
    new HardSourceWebpackPlugin(),
  ],
);
