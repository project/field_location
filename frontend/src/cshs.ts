const behavior: string = 'locationFieldsCshs';

Drupal.behaviors[behavior] = {
  attach(context, settings): void {
    const $elements = jQuery('.simpler-select-root').nextAll('.select-wrapper').find('select');
    // Give user a chance to choose their location.
    const deferred = jQuery.Deferred().fail(() => $elements.prop('disabled', false));

    // Disable editing.
    $elements.prop('disabled', true);

    navigator.geolocation.getCurrentPosition(({ coords }) => {
      const me = new google.maps.LatLng(coords.latitude, coords.longitude);

      jQuery.each(settings[behavior], (fieldName: string, items: object): void => {
        jQuery.each(items, (htmlID: string, terms: Record<string, string>): void => {
          jQuery.each(terms, (termID: string, coordinates: string): void | false => {
            if (google.maps.geometry.poly.containsLocation(me, new google.maps.Polygon({ paths: JSON.parse(coordinates) }))) {
              jQuery('#' + htmlID)
                .val(termID)
                .data('plugin_simplerSelect')
                .init();

              deferred.resolve(me);

              return false;
            }
          });
        });
      });

      deferred.reject();
    });
  }
};
