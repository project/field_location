/**
 * @see misc/drupal.es6.js
 */
declare namespace Drupal {
  type Context = HTMLDocument | HTMLElement;
  type Trigger = 'unload' | 'move' | 'serialize';

  interface Settings {
    [key: string]: any;
    [key: number]: any;
  }

  interface Behavior extends Settings {
    attach: (context: Context, settings: Settings) => void;
    detach?: (context: Context, settings: Settings, trigger?: Trigger) => void;
  }

  const behaviors: Record<string, Behavior>;
  function t(string: string, context?: Record<string, string | number>): string;
}

declare class drupalSettings implements Drupal.Settings {}
