export abstract class AbstractCoordinates<T extends google.maps.Marker | google.maps.Polygon> {
  public static readonly type: string;
  public static readonly zoom: number;
  protected readonly map: google.maps.Map;
  private bounds: google.maps.LatLngBounds;

  public constructor(protected readonly field: HTMLTextAreaElement, protected readonly indicator: T) {
    this.map = <google.maps.Map> indicator.getMap();

    if (this.field.value) {
      try {
        // Center the map at initialization.
        this.focus(this.setPath(JSON.parse(this.field.value)));
      }
      catch (error) {
        // The "JSON.parse()" may throw.
        console.error(error);
      }
    }
  }

  public changePlace(location: google.maps.LatLng): void {
    this.setPlace(location);
    // Center the map after changing the place.
    this.focus(this.update());
  }

  public update(): google.maps.LatLngLiteral[] {
    const path = this
      .getPath()
      .map((position) => position.toJSON());

    this.field.value = JSON.stringify(path);

    return this.setPath(path);
  }

  private focus(path: google.maps.LatLngLiteral[]): void {
    if (this.bounds) {
      this.setMapCenter(this.bounds, path);
      this.map.setZoom((<typeof AbstractCoordinates> this.constructor).zoom);
    } else {
      console.warn('The map can be focused only when the area path is set.');
    }
  }

  protected setPath<T extends google.maps.LatLngLiteral[]>(path: T): T {
    if (path.length > 0) {
      this.bounds = path.reduce((bounds, position) => bounds.extend(position), new google.maps.LatLngBounds());
      this.setPosition(this.bounds, path);
    }

    return path;
  }

  /**
   * Returns a path to the area indicator after user modifications.
   */
  protected abstract getPath(): google.maps.LatLng[];

  /**
   * Sets the position of the area indicator in the following cases:
   * - initialization;
   * - search result selection;
   * - form submission.
   */
  protected abstract setPosition(bounds: google.maps.LatLngBounds, path: google.maps.LatLngLiteral[]): void;

  /**
   * Sets a center of the map in the following cases:
   * - initialization;
   * - search result selection;
   */
  protected abstract setMapCenter(bounds: google.maps.LatLngBounds, path: google.maps.LatLngLiteral[]): void;

  /**
   * Switches map area to the place selected from search results dropdown.
   */
  protected abstract setPlace(location: google.maps.LatLng): void;
}
