export function getOverpassBoundaries(bounds: google.maps.LatLngBounds) {
  const deferred = <JQuery.Deferred<google.maps.LatLngLiteral[]>> jQuery.Deferred();

  /** @link http://lxbarth.com/bbox */
  jQuery.getJSON('https://www.overpass-api.de/api/interpreter?data=[out:json];node(' + getBoundingBox(bounds) + ');out;', ({ elements }): void => {
    elements.pop();

    deferred.resolve(
      elements
        .filter(({ lat, lon }): boolean => lat && lon)
        .map(({ lat, lon: lng }) => ({ lat, lng })),
    );
  });

  return deferred;
}

/**
 * @link http://wiki.openstreetmap.org/wiki/Bounding_Box
 */
export function getBoundingBox(bounds: google.maps.LatLngBounds): string {
  const southwest = bounds.getSouthWest();
  const northeast = bounds.getNorthEast();
  const southwestLatitude = southwest.lat();
  const southwestLongitude = southwest.lng();
  const northeastLatitude = northeast.lat();
  const northeastLongitude = northeast.lng();

  return [
    Math.min.apply(Math, [southwestLatitude, northeastLatitude]),
    Math.min.apply(Math, [southwestLongitude, northeastLongitude]),
    Math.max.apply(Math, [southwestLatitude, northeastLatitude]),
    Math.max.apply(Math, [southwestLongitude, northeastLongitude])
  ].join(',');
}
