import { AbstractCoordinates } from './AbstractCoordinates';

export class MarkerCoordinates<T extends google.maps.Marker> extends AbstractCoordinates<T> {
  public static readonly type = 'marker';
  public static readonly zoom = 17;

  public constructor(field: HTMLTextAreaElement, marker: T) {
    super(field, marker);

    let clickTimer;

    this.map.addListener('click', (event) => {
      // Make sure it's a single click.
      clickTimer = setTimeout(() => this.setPlace(event.latLng), 250);
    });

    this.map.addListener('dblclick', () => clearTimeout(clickTimer));
  }

  protected getPath(): google.maps.LatLng[] {
    return [<google.maps.LatLng> this.indicator.getPosition()];
  }

  protected setPosition(bounds: google.maps.LatLngBounds, path: google.maps.LatLngLiteral[]): void {
    this.setPlace(bounds.getCenter());
  }

  protected setMapCenter(bounds: google.maps.LatLngBounds, path: google.maps.LatLngLiteral[]): void {
    this.map.setCenter(bounds.getCenter());
  }

  protected setPlace(location: google.maps.LatLng): void {
    this.indicator.setPosition(location);
  }
}
