import { AbstractCoordinates } from './AbstractCoordinates';

export class PolygonCoordinates<T extends google.maps.Polygon> extends AbstractCoordinates<T> {
  public static readonly type = 'polygon';
  public static readonly zoom = 15;

  protected setPath<T extends google.maps.LatLngLiteral[]>(path: T): T {
    // This occurs when a user switched field type from "marker" to "polygon".
    // The polygon cannot exist with a single point of coordinates so we add a
    // dummy one so a user can draw what's needed.
    if (path.length === 1) {
      path.push(<google.maps.LatLngLiteral> {
        lat: path[0].lat - 0.005,
        lng: path[0].lng - 0.005,
      });
    }

    return super.setPath(path);
  }

  protected getPath(): google.maps.LatLng[] {
    return this.indicator.getPath().getArray();
  }

  protected setPosition(bounds: google.maps.LatLngBounds, path: google.maps.LatLngLiteral[]): void {
    this.indicator.setPath(path);
  }

  protected setMapCenter(bounds: google.maps.LatLngBounds, path: google.maps.LatLngLiteral[]): void {
    this.map.fitBounds(bounds);
  }

  protected setPlace(location: google.maps.LatLng): void {
    const bounds = new google.maps.Circle({ center: location, radius: 250 }).getBounds();
    this.indicator.setPath([bounds.getNorthEast(), bounds.getSouthWest()]);
  }
}
