import { PolygonCoordinates } from './coordinates/PolygonCoordinates';
import { MarkerCoordinates } from './coordinates/MarkerCoordinates';
import './css/field_location.scss';

const geoPositionButtonLabel: string = Drupal.t('Go to your current geoposition');

const defaultMapOptions: Readonly<google.maps.MapOptions> = {
  // Disable switching between road map, landscape and other types.
  mapTypeControl: false,
  // Disable zooming on scrolling mouse wheel.
  scrollwheel: false,
  zoom: 4,
  // This is a geographical center of Earth.
  /** @link https://en.wikipedia.org/wiki/Geographical_centre_of_Earth */
  center: {
    lat: 39,
    lng: 34,
  },
};

const defaultPolygonOptions: Readonly<google.maps.PolygonOptions> = {
  strokeOpacity: 0.5,
  strokeWeight: 2,
  fillOpacity: 0.1,
  strokeColor: '#FF0000',
  fillColor: '#FF0000',
  editable: true,
};

type LocationType = 'marker' | 'polygon';

interface Options {
  locality: string;
  // Stringified "google.maps.LatLng[]".
  coordinates: string;
  location_type: LocationType;
  googlemaps_library: string;
}

function getCoordinates(type: LocationType, map: google.maps.Map, coordinatesTextarea: HTMLTextAreaElement) {
  type = type || PolygonCoordinates.type;

  switch (type) {
    case PolygonCoordinates.type:
      return new PolygonCoordinates(coordinatesTextarea, new google.maps.Polygon({ ...defaultPolygonOptions, map }));

    case MarkerCoordinates.type:
      return new MarkerCoordinates(coordinatesTextarea, new google.maps.Marker({ map }));
  }

  throw new Error('The ' + type + ' is an invalid location type. Please update filed settings.');
}

function getLocalityUpdater(search: HTMLInputElement, storage: HTMLTextAreaElement) {
  return <T extends string> (value: T): T => (storage.value = search.value = value);
}

jQuery.each(drupalSettings['locationFields'], (fieldName: string, settings) => {
  document.querySelectorAll('.field-location.' + String(fieldName)).forEach((container: HTMLFieldSetElement, i: number) => {
    const options = <Options> settings[i];
    const searchInput = <HTMLInputElement> container.querySelector('.search');
    const changeLocality = getLocalityUpdater(searchInput, <HTMLTextAreaElement> container.querySelector('.locality'));
    const geocoder = new google.maps.Geocoder();
    const search = new google.maps.places.Autocomplete(searchInput);
    const map = new google.maps.Map(<HTMLDivElement> container.querySelector('.map'), defaultMapOptions);
    const coordinates = getCoordinates(options.location_type, map, <HTMLTextAreaElement> container.querySelector('.coordinates'));
    const changePlace = (location: google.maps.LatLng, locality: string): void => {
      changeLocality(locality);
      coordinates.changePlace(location);
    };

    changeLocality(options.locality);

    // Add "Find me" button.
    jQuery(`<div class="location-button" title="${geoPositionButtonLabel}">`).each((i: number, element: HTMLDivElement) => {
      element.addEventListener('click', (event) => {
        event.preventDefault();
        element.classList.add('active');

        navigator.geolocation.getCurrentPosition(({ coords }) => {
          const location = new google.maps.LatLng(coords.latitude, coords.longitude);

          geocoder.geocode({ location }, (results, status) => {
            if (status === 'OK') {
              const { formatted_address } = results.shift();
              changePlace(location, formatted_address);
            }

            element.classList.remove('active');
          });
        });
      });

      map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(element);
    });

    // Read polygon points before form submission.
    (<HTMLFormElement> container.closest('form'))
      .addEventListener('submit', () => coordinates.update());

    // Prevent form submit when "return" button clicked.
    searchInput.addEventListener('keydown', (event) => {
      if (13 === event.which) {
        event.preventDefault();
      }
    });

    // Handle places autocomplete.
    search.addListener('place_changed', () => {
      const place = search.getPlace();

      if (place.geometry) {
        changePlace(place.geometry.location, searchInput.value);
      }
    });
  });
});
