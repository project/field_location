<?php

namespace Drupal\field_location\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the field type.
 *
 * @FieldType(
 *   id = "location",
 *   label = @Translation("Location"),
 *   default_widget = "location_widget_standard",
 *   default_formatter = "location_formatter_standard",
 * )
 */
class Location extends FieldItemBase {

  public const LOCATION_POLYGON = 'polygon';
  public const LOCATION_MARKER = 'marker';
  public const DEFAULT_LIBRARY = 'field_location';

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field): array {
    return [
      'columns' => self::columns(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return [
      'location_type' => static::LOCATION_POLYGON,
      'googlemaps_library' => static::DEFAULT_LIBRARY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {
    $element = [];

    $element['googlemaps_library'] = [
      '#type' => 'googlemaps_autocomplete',
      '#title' => $this->t('Google Maps instance'),
      '#required' => TRUE,
      '#default_value' => $this->getSetting('googlemaps_library'),
    ];

    $element['location_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Location type'),
      '#required' => TRUE,
      '#default_value' => $this->getSetting('location_type'),
    ];

    foreach ([
      self::LOCATION_POLYGON => [
        $this->t('Polygon'),
        $this->t('Draw a polygon containing an area'),
      ],
      self::LOCATION_MARKER => [
        $this->t('Marker'),
        $this->t('Select a single location to store'),
      ],
    ] as $key => [$title, $description]) {
      $element['location_type']['#options'][$key] = $title;
      $element['location_type'][$key]['#description'] = $description;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field): array {
    $properties = [];

    foreach (self::columns() as $column => $definition) {
      $properties[$column] = DataDefinition::create('string')
        ->setLabel($definition['label'])
        ->setSetting('default_value', $definition['default'] ?? '')
        ->setReadOnly(TRUE)
        ->setRequired(TRUE);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName(): ?string {
    // Location has no main property.
    return NULL;
  }

  /**
   * Returns the field's columns.
   *
   * @return array[]
   *   The field's columns.
   */
  private static function columns(): array {
    $columns = [];

    $columns['locality'] = [
      'type' => 'varchar',
      'label' => \t('Locality'),
      'length' => 256,
      'default' => '',
      'not null' => FALSE,
    ];

    $columns['coordinates'] = [
      'type' => 'blob',
      'size' => 'big',
      'label' => \t('Coordinates'),
    ];

    return $columns;
  }

}
