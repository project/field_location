<?php

namespace Drupal\field_location\Plugin\Field\FieldWidget\Location;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Defines the field widget.
 *
 * @FieldWidget(
 *   id = "location_widget_standard",
 *   label = @Translation("Location (standard)"),
 *   field_types = {
 *     "location",
 *   },
 * )
 */
class Standard extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    if ($library = $this->getFieldSetting('googlemaps_library')) {
      $element['#attached']['library'][] = \googlemaps_library($library);
      $element['#attached']['library'][] = 'field_location/google-maps-preview';
    }
    else {
      $error = $this->t('The storage settings of %field_name missing the Google Maps library definition. The field cannot be used until this is resolved.', [
        '%field_name' => $this->fieldDefinition->getLabel(),
      ]);

      $this
        ->messenger()
        ->addError($error);

      $element['error'] = [
        '#tag' => 'strong',
        '#type' => 'html_tag',
        '#value' => $error,
        '#attributes' => [
          'class' => ['form-item--error-message'],
        ],
      ];

      return $element;
    }

    $definition = $items->getFieldDefinition();
    $is_required = $definition->isRequired();
    $field_name = $items->getName();
    $settings = $definition->getSettings();
    $defaults = $definition->getDefaultValueLiteral();
    $values = $items->getValue();
    $field = $definition->getFieldStorageDefinition();

    $element += [
      '#type' => 'fieldset',
      '#title' => $field->getLabel(),
      '#attributes' => [
        'class' => ['field-location', $field_name],
      ],
    ];

    $element['preview'] = [
      '#theme' => 'google-map-preview',
    ];

    foreach ($field->getPropertyNames() as $name) {
      $property = $field->getPropertyDefinition($name);
      \assert($property !== NULL);

      // Use configured value.
      if (isset($values[$delta][$name])) {
        $value = $values[$delta][$name];
      }
      // Use default if set.
      elseif (isset($defaults[$delta][$name])) {
        $value = $defaults[$delta][$name];
      }
      // Use default value from field type definition.
      else {
        $value = $property->getSetting('default_value');
      }

      $element[$name] = [
        '#type' => 'textarea',
        '#title' => $property->getLabel(),
        '#required' => $is_required,
        '#default_value' => $value,
        '#attributes' => [
          'class' => ['data-container', $name],
          'readonly' => $property->isReadOnly(),
        ],
        '#attached' => [
          'drupalSettings' => [
            'locationFields' => [
              $field_name => [
                $delta => $settings + [
                  $name => $value,
                ],
              ],
            ],
          ],
        ],
      ];
    }

    return $element;
  }

}
