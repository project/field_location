<?php

namespace Drupal\Tests\field_location\Functional;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\field_location\Plugin\Field\FieldType\Location;
use Drupal\googlemaps\Entity\GoogleMap;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the location field.
 *
 * @group field_location
 */
class ManageDisplayTest extends BrowserTestBase {

  use FieldUiTestTrait;

  protected const FIELD_TYPE = 'location';
  protected const FIELD_NAME = 'field_location_test';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'block',
    'field_ui',
    'field_location',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * The content type for testing.
   *
   * @var \Drupal\node\NodeTypeInterface
   */
  protected $contentType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('system_breadcrumb_block');

    $this->drupalLogin($this->drupalCreateUser([
      'access content',
      'bypass node access',
      'administer node fields',
      'administer node display',
      'administer node form display',
      'administer content types',
    ]));

    $this->contentType = $this->drupalCreateContentType([
      'type' => $this->randomMachineName(),
    ]);
  }

  /**
   * Tests field creation.
   */
  public function testCreateField(): void {
    $this->fieldUIAddNewField('admin/structure/types/manage/' . $this->contentType->id(), static::FIELD_NAME, NULL, static::FIELD_TYPE, [
      'settings[location_type]' => Location::LOCATION_POLYGON,
      'settings[googlemaps_library]' => EntityAutocomplete::getEntityLabels(
        $this->container->get('entity_type.manager')->getStorage(GoogleMap::ENTITY_TYPE)->loadMultiple([Location::DEFAULT_LIBRARY])
      ),
    ]);
    // @todo Continue here.
  }

}
